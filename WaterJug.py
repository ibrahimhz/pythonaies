j1 = 0
j2 = 0 
x = 4
y = 3

print("initial state = (0, 0)")
print("Capacities = (4, 3)")
print("Goal state = (2, 0)")
while j1 != 2:
    r = int(input("Enter rule"))
    if(r == 1):
        j1 = x
    elif(r == 2):
        j2 = y
    elif(r == 3):
        j1 = 0
    elif(r == 4):
        j2 = 0
    elif(r == 5):  # pour from j1 to j2 and j1 has some remaining 
        t = y - j2
        j2 = y
        j1 = j1 - t    
        if j1 < 0:
            j1 = 0

    elif(r == 6): # pour from j2 to j1 when j1 is having some water such as 1 or 2 or 3
        t = x - j1    
        j1 = x        
        j2 = j2 - t   
        if j2 < 0:
            j2 = 0
    
    elif(r == 7): # pour all j1 to j2 until j1 become empty 0
        j2 = j2 + j1
        j1 = 0
        if j2 > y:
            j2 = y
    
    elif (r == 8): # pour all j2 to j1 until j2 become empty 0
        j1 = j1 + j2
        j2 = 0
        if j1 > x:
            j1 = x
    
    print(j1, j2)
