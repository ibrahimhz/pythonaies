from sklearn.datasets import make_classification

X,Y = make_classification(
    n_features= 6,
    n_classes= 3,
    n_samples= 800,
    n_informative= 2,
    random_state= 1,
    n_clusters_per_class= 1,
)

import matplotlib.pyplot as plt
plt.scatter(X[:, 0], X[:, 1], c=Y, marker="*");

# spliting the model into train and test
from sklearn.model_selection import train_test_split
X_train, X_test, Y_train, Y_test = train_test_split(
    X, Y, test_size=0.33, random_state=125
)

from sklearn.naive_bayes import GaussianNB
# build a gauassian classifier
model = GaussianNB()

# model training
model.fit(X_train, Y_train)

# predict output
predicted = model.predict([X_test[6]])
print("Actual Value: ", Y_test[6])
print("Predicted Value: ", predicted[0])

from sklearn.metrics import (
    accuracy_score,
    f1_score
)

Y_pred = model.predict(X_test)
accuray = accuracy_score(Y_pred, Y_test)
f1 = f1_score(Y_pred, Y_test, average="weighted")
print("Accuracy: ", accuray)
print("F1 Score: ", f1)
