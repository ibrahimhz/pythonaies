# graph coloring

def graphcoloring(graph):
    vertices = sorted(list(graph.keys()))
    colour_graph = {}

    for vertex in vertices:
        unused_colors = len(vertices) * [True]
        
        for neighbor in graph[vertex]:
            if neighbor in colour_graph:
                colour = colour_graph[neighbor]
                unused_colors[colour] = False
        for colour, unused in enumerate(unused_colors):
            if unused:
                colour_graph[vertex] = colour
                break
    
    return colour_graph



graph = {
    'a': ['b', 'c', 'd'],
    'b': ['a', 'c', 'e'],
    'c': ['a', 'b'],
    'd': ['a', 'e'],
    'e': ['b', 'd']
}

result = graphcoloring(graph)
print(result)
